﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpClient
{
    public class Client<TRequester, TSerializer> : IClient<TRequester, TSerializer> where TRequester : IRequester, new() where TSerializer : ISerializer, new()
    {
        IRequester requester;

        IResponseHandler responseHandler;

        ISerializer serializer;

        public Client(IResponseHandler responseHandler, Dictionary<string, FunctionInfo> functions = null)
        {
            this.requester = new TRequester();
            this.responseHandler = responseHandler;
            this.serializer = new TSerializer();
            Functions = functions;
        }

        public Dictionary<string, FunctionInfo> Functions;
        public void Request<TData>(string function, TData data) 
        {
            this.Request(function, serializer.Serialize(data));
        }

        public void Request(string function, byte[] data) 
        {
            FunctionInfo info; 
            try
            {
                info = Functions[function];
            }
            catch (Exception)
            {
                throw new Exception("No such function");
            }

            var response = requester.DoRequest(info.Url, info.Method, data, info.ContentType);

            if (responseHandler != null)
            {
                responseHandler.Handle(response);
            }
        }

        public void Request(string function, string data) 
        {
            this.Request(function, Encoding.UTF8.GetBytes(data));
        }

        public void Request(string function)
        {
            var info = Functions[function];
            var response = requester.DoRequest(info.Url, info.Method, null, info.ContentType);
            responseHandler.Handle(response);
        }

        public void AddFunction(string function, FunctionInfo info)
        {
            if (Functions == null)
                Functions = new Dictionary<string, FunctionInfo>();

            Functions.Add(function, info);
        }

        public void AddFunction(string function, Uri url, string method = "GET", string contentType = "application/json")
        {
            AddFunction(function, new FunctionInfo(url, method, contentType));
        }
    }
}
