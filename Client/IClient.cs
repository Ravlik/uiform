﻿namespace HttpClient
{
    public interface IClient<TRequester, TSerializer> where TRequester: IRequester, new() where TSerializer: ISerializer, new()
    {
        void Request<TData>(string function, TData data);
        void Request(string function, byte[] data);
        void Request(string function, string data);
        void Request(string function);
    }
}