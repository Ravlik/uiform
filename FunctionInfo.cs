﻿using System;

namespace HttpClient
{
    public class FunctionInfo
    {
        public Uri Url { get; set; }
        public string Method { get; set; }
        public string ContentType { get; set; }

        public FunctionInfo(Uri url, string method, string contentType)
        {
            Url = url;
            Method = method;
            ContentType = contentType;
        }

        public FunctionInfo(string url, string method, string contentType)
        {
            Url = new Uri(url);
            Method = method;
            ContentType = contentType;
        }
    }
}