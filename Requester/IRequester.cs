﻿using System;
using System.Net;

namespace HttpClient
{
    public interface IRequester
    {
        HttpWebResponse DoRequest(string url, string method, byte[] data, string dataContentType);
        HttpWebResponse DoRequest(Uri url, string method, byte[] data, string dataContentType);
    }
}
