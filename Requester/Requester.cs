﻿using System;
using System.Net;

namespace HttpClient
{
    public class Requester : IRequester
    {
        public HttpWebResponse DoRequest(string uri, string method, byte[] data, string dataContentType)
        {
            return DoRequest(new Uri(uri), method, data, dataContentType);
        }

        public HttpWebResponse DoRequest(Uri uri, string method, byte[] data, string dataContentType)
        {
            var request = HttpWebRequest.Create(uri);
            request.Method = method;

            if (data != null)
            {
                request.ContentType = dataContentType;
                request.ContentLength = data.Length;

                using (var rstream = request.GetRequestStream())
                {
                    rstream.Write(data, 0, data.Length);
                }
            }

            return (HttpWebResponse)request.GetResponse();
        }
    }
}
