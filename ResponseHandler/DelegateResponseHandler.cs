﻿using System.Net;

namespace HttpClient
{
    public class DelegateResponseHandler<TResult, TSerializer> : IResponseHandler where TSerializer: ISerializer, new()
    {
        public delegate void Handler<T>(TSerializer serializer, HttpWebResponse response);

        public Handler<TResult> ResponseHandler;
        public void Handle(HttpWebResponse response)
        {
            ResponseHandler(new TSerializer(), response);
        }
    }
}