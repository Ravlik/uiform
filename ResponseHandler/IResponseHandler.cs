﻿using System.Net;

namespace HttpClient
{
    public interface IResponseHandler
    {
        void Handle(HttpWebResponse response);
    }
}