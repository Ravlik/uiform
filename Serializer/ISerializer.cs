﻿namespace HttpClient
{
    public interface ISerializer
    {
        string Serialize<TData>(TData data);
        TData Deserialize<TData>(string str);
    }
}