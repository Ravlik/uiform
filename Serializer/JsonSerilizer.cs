﻿using Newtonsoft.Json;

namespace HttpClient
{
    public class JsonSerilizer : ISerializer
    {
        public string Serialize<TData>(TData data)
        {
            return JsonConvert.SerializeObject(data);
        }

        public TData Deserialize<TData>(string str)
        {
            return JsonConvert.DeserializeObject<TData>(str);
        }
    }
}