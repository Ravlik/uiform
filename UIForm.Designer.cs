﻿namespace HttpClient
{
    partial class UIForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Api_Manager_ViberBot_Put_Btn = new System.Windows.Forms.Button();
            this.Api_Manager_ViberBot_Post_Btn = new System.Windows.Forms.Button();
            this.Api_Manager_ViberBot_ID_Get_Btn = new System.Windows.Forms.Button();
            this.Api_Manager_ViberBot_ID_Delete_Btn = new System.Windows.Forms.Button();
            this.Api_License_ViberBot_Post_Btn = new System.Windows.Forms.Button();
            this.Api_Manager_ViberBot_Id_Get_TB = new System.Windows.Forms.TextBox();
            this.Api_Manager_ViberBot_Id_Delete_TB = new System.Windows.Forms.TextBox();
            this.ResultRTB = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // Api_Manager_ViberBot_Put_Btn
            // 
            this.Api_Manager_ViberBot_Put_Btn.Location = new System.Drawing.Point(12, 12);
            this.Api_Manager_ViberBot_Put_Btn.Name = "Api_Manager_ViberBot_Put_Btn";
            this.Api_Manager_ViberBot_Put_Btn.Size = new System.Drawing.Size(207, 23);
            this.Api_Manager_ViberBot_Put_Btn.TabIndex = 1;
            this.Api_Manager_ViberBot_Put_Btn.Text = "Api/Manager/ViberBot PUT";
            this.Api_Manager_ViberBot_Put_Btn.UseVisualStyleBackColor = true;
            this.Api_Manager_ViberBot_Put_Btn.Click += new System.EventHandler(this.Api_Manager_ViberBot_Put_Btn_Click);
            // 
            // Api_Manager_ViberBot_Post_Btn
            // 
            this.Api_Manager_ViberBot_Post_Btn.Location = new System.Drawing.Point(12, 42);
            this.Api_Manager_ViberBot_Post_Btn.Name = "Api_Manager_ViberBot_Post_Btn";
            this.Api_Manager_ViberBot_Post_Btn.Size = new System.Drawing.Size(207, 23);
            this.Api_Manager_ViberBot_Post_Btn.TabIndex = 2;
            this.Api_Manager_ViberBot_Post_Btn.Text = "Api/Manager/VinerBot POST";
            this.Api_Manager_ViberBot_Post_Btn.UseVisualStyleBackColor = true;
            this.Api_Manager_ViberBot_Post_Btn.Click += new System.EventHandler(this.Api_Manager_ViberBot_Post_Btn_Click);
            // 
            // Api_Manager_ViberBot_ID_Get_Btn
            // 
            this.Api_Manager_ViberBot_ID_Get_Btn.Location = new System.Drawing.Point(12, 72);
            this.Api_Manager_ViberBot_ID_Get_Btn.Name = "Api_Manager_ViberBot_ID_Get_Btn";
            this.Api_Manager_ViberBot_ID_Get_Btn.Size = new System.Drawing.Size(207, 23);
            this.Api_Manager_ViberBot_ID_Get_Btn.TabIndex = 3;
            this.Api_Manager_ViberBot_ID_Get_Btn.Text = "Api/Manager/ViberBot/{Id} GET";
            this.Api_Manager_ViberBot_ID_Get_Btn.UseVisualStyleBackColor = true;
            this.Api_Manager_ViberBot_ID_Get_Btn.Click += new System.EventHandler(this.Api_Manager_ViberBot_ID_Get_Btn_Click);
            // 
            // Api_Manager_ViberBot_ID_Delete_Btn
            // 
            this.Api_Manager_ViberBot_ID_Delete_Btn.Location = new System.Drawing.Point(12, 101);
            this.Api_Manager_ViberBot_ID_Delete_Btn.Name = "Api_Manager_ViberBot_ID_Delete_Btn";
            this.Api_Manager_ViberBot_ID_Delete_Btn.Size = new System.Drawing.Size(207, 23);
            this.Api_Manager_ViberBot_ID_Delete_Btn.TabIndex = 4;
            this.Api_Manager_ViberBot_ID_Delete_Btn.Text = "Api/Manager/ViberBot/{Id} DELETE";
            this.Api_Manager_ViberBot_ID_Delete_Btn.UseVisualStyleBackColor = true;
            this.Api_Manager_ViberBot_ID_Delete_Btn.Click += new System.EventHandler(this.Api_Manager_ViberBot_ID_Delete_Btn_Click);
            // 
            // Api_License_ViberBot_Post_Btn
            // 
            this.Api_License_ViberBot_Post_Btn.Location = new System.Drawing.Point(12, 130);
            this.Api_License_ViberBot_Post_Btn.Name = "Api_License_ViberBot_Post_Btn";
            this.Api_License_ViberBot_Post_Btn.Size = new System.Drawing.Size(207, 23);
            this.Api_License_ViberBot_Post_Btn.TabIndex = 5;
            this.Api_License_ViberBot_Post_Btn.Text = "Api/License/ViberBot POST";
            this.Api_License_ViberBot_Post_Btn.UseVisualStyleBackColor = true;
            this.Api_License_ViberBot_Post_Btn.Click += new System.EventHandler(this.Api_License_ViberBot_Post_Btn_Click);
            // 
            // Api_Manager_ViberBot_Id_Get_TB
            // 
            this.Api_Manager_ViberBot_Id_Get_TB.Location = new System.Drawing.Point(225, 74);
            this.Api_Manager_ViberBot_Id_Get_TB.Name = "Api_Manager_ViberBot_Id_Get_TB";
            this.Api_Manager_ViberBot_Id_Get_TB.Size = new System.Drawing.Size(110, 20);
            this.Api_Manager_ViberBot_Id_Get_TB.TabIndex = 6;
            // 
            // Api_Manager_ViberBot_Id_Delete_TB
            // 
            this.Api_Manager_ViberBot_Id_Delete_TB.Location = new System.Drawing.Point(225, 103);
            this.Api_Manager_ViberBot_Id_Delete_TB.Name = "Api_Manager_ViberBot_Id_Delete_TB";
            this.Api_Manager_ViberBot_Id_Delete_TB.Size = new System.Drawing.Size(110, 20);
            this.Api_Manager_ViberBot_Id_Delete_TB.TabIndex = 7;
            // 
            // ResultRTB
            // 
            this.ResultRTB.Location = new System.Drawing.Point(12, 160);
            this.ResultRTB.Name = "ResultRTB";
            this.ResultRTB.Size = new System.Drawing.Size(324, 247);
            this.ResultRTB.TabIndex = 8;
            this.ResultRTB.Text = "";
            // 
            // UIForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 450);
            this.Controls.Add(this.ResultRTB);
            this.Controls.Add(this.Api_Manager_ViberBot_Id_Delete_TB);
            this.Controls.Add(this.Api_Manager_ViberBot_Id_Get_TB);
            this.Controls.Add(this.Api_License_ViberBot_Post_Btn);
            this.Controls.Add(this.Api_Manager_ViberBot_ID_Delete_Btn);
            this.Controls.Add(this.Api_Manager_ViberBot_ID_Get_Btn);
            this.Controls.Add(this.Api_Manager_ViberBot_Post_Btn);
            this.Controls.Add(this.Api_Manager_ViberBot_Put_Btn);
            this.Name = "UIForm";
            this.Text = "UIForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Api_Manager_ViberBot_Put_Btn;
        private System.Windows.Forms.Button Api_Manager_ViberBot_Post_Btn;
        private System.Windows.Forms.Button Api_Manager_ViberBot_ID_Get_Btn;
        private System.Windows.Forms.Button Api_Manager_ViberBot_ID_Delete_Btn;
        private System.Windows.Forms.Button Api_License_ViberBot_Post_Btn;
        private System.Windows.Forms.TextBox Api_Manager_ViberBot_Id_Get_TB;
        private System.Windows.Forms.TextBox Api_Manager_ViberBot_Id_Delete_TB;
        private System.Windows.Forms.RichTextBox ResultRTB;
    }
}