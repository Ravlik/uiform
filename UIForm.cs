﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.ComponentModel.DataAnnotations;


namespace HttpClient
{
    public partial class UIForm : Form
    {
        //Model - тип результата запроса, JsonSerializer передаем как тип, поскольку нам нет дела до его внутренних переживаний
        private DelegateResponseHandler<Model, JsonSerilizer> responseHandler = new DelegateResponseHandler<Model, JsonSerilizer>();

        Client<Requester, JsonSerilizer> client;

        private static string ResponceStr;

        private static string connectionString = "10.0.0.16:5081/";



        private Dictionary<string,FunctionInfo> functions = new Dictionary<string, FunctionInfo>()
        {
            ["ViberBot/GET"] = new FunctionInfo($"{connectionString}/api/Manager/ViberBot", "GET", "application/json"),
            ["ViberBot/PUT"] = new FunctionInfo($"{connectionString}/api/Manager/ViberBot", "PUT", "application/json"),
            ["ViberBot/POST"] = new FunctionInfo($"{connectionString}/api/Manager/ViberBot", "POST", "application/json"),
            ["ViberBot/DELETE"] = new FunctionInfo($"{connectionString}/api/Manager/ViberBot/", "GET", "application/json"),
            ["License/POST"] = new FunctionInfo($"{connectionString}/api/License/ViberBot/", "POST", "application/json")
        };

        class Model
        {
            [Required]
            public int Id { get; set; }

            [Required]
            public string Name { get; set; }
        }

        public UIForm()
        {
            InitializeComponent();

            //обработка результата в делегате, позже мб стоит реализовать отдельные классы
            responseHandler.ResponseHandler += (serializer, response) => Handler(serializer, response);

            client = new Client<Requester, JsonSerilizer>(responseHandler, functions);
        }

        // Логика хэндлера практически не изменена, единственное - Responce записывается в статическое поле, 
        // затем выводится
        private static void Handler(ISerializer serializer, HttpWebResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                // логика с ОК

                if (response.ContentLength > 0)
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            var str = reader.ReadToEnd();
                            ResponceStr = str;
                            var data = serializer.Deserialize<Model>(str);

                            try
                            {
                                Validator.ValidateObject(data, new ValidationContext(new Model { Id = 1, Name = "asd" }));
                            }
                            catch (Exception)
                            {
                                throw new Exception("Validation failed");
                            }

                            var result = (Model)data;
                        }
                    }
                }
            }
        }

        private void Api_Manager_ViberBot_ID_Get_Btn_Click(object sender, EventArgs e)
        {
            if (Api_Manager_ViberBot_ID_Get_Btn.Text == "")
            {
                client.Request("ViberBot/GET");
                ResultRTB.Text = ResponceStr;
            }
            else
            {
                client.AddFunction($"ViberBot/GET/{Api_Manager_ViberBot_Id_Get_TB.Text}",
                    new FunctionInfo($"{connectionString}/api/Manager/ViberBot/{Api_Manager_ViberBot_Id_Get_TB.Text}",
                        "GET",
                        "application/json"));
                client.Request($"ViberBot/GET/{Api_Manager_ViberBot_Id_Get_TB.Text}");
                client.Functions.Remove($"ViberBot/GET/{Api_Manager_ViberBot_Id_Get_TB.Text}");
            }
            ResultRTB.Text = ResponceStr;
        }

        private void Api_Manager_ViberBot_Post_Btn_Click(object sender, EventArgs e)
        {
            client.Request("ViberBot/POST");
            ResultRTB.Text = ResponceStr;
        }

        private void Api_Manager_ViberBot_Put_Btn_Click(object sender, EventArgs e)
        {
            client.Request("ViberBot/PUT");
            ResultRTB.Text = ResponceStr;
        }

        private void Api_License_ViberBot_Post_Btn_Click(object sender, EventArgs e)
        {
            client.Request("License/POST");
            ResultRTB.Text = ResponceStr;
        }

        private void Api_Manager_ViberBot_ID_Delete_Btn_Click(object sender, EventArgs e)
        {
            if (Api_Manager_ViberBot_ID_Get_Btn.Text == "")
            {
                MessageBox.Show("Введите ID");
                ResultRTB.Text = "";
            }
            else
            {
                client.AddFunction($"ViberBot/DELETE/{Api_Manager_ViberBot_Id_Delete_TB.Text}",
                    new FunctionInfo($"{connectionString}/api/Manager/ViberBot/{Api_Manager_ViberBot_Id_Delete_TB.Text}",
                        "DELETE",
                        "application/json"));
                client.Request($"ViberBot/DELETE/{Api_Manager_ViberBot_Id_Delete_TB.Text}");
                client.Functions.Remove($"ViberBot/DELETE/{Api_Manager_ViberBot_Id_Delete_TB.Text}");
                ResultRTB.Text = ResponceStr;
            }
        }
    }
}
