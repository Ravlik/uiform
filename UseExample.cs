﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HttpClient
{
    public static class UseExample
    {
        class Model
        {
            [Required]
            public int Id { get; set; }

            [Required]
            public string Name { get; set; }
        }

        private static void Handler(ISerializer serializer, HttpWebResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                // логика с ОК

                if (response.ContentLength > 0)
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            var str = reader.ReadToEnd();
                            var data = serializer.Deserialize<Model>(str);

                            try
                            {
                                Validator.ValidateObject(data, new ValidationContext(new Model { Id = 1, Name = "asd" }));
                            }
                            catch (Exception)
                            {
                                throw new Exception("Validation failed");
                            }

                            var result = (Model)data;
                            //Console.WriteLine($"{result.Id} | {result.Name}");
                        }
                    }
                }  
            }
        }
        
        class Input
        {
            public int Id;
            public string Namae; 
        }

        public static void Main(string[] args)
        {
            //Model - тип результата запроса, JsonSerializer передаем как тип, поскольку нам нет дела до его внутренних переживаний
            var responseHandler = new DelegateResponseHandler<Model, JsonSerilizer>();

            //обработка результата в делегате, позже мб стоит реализовать отдельные классы
            responseHandler.ResponseHandler += (serializer, response) => Handler(serializer, response);

            var functions = new Dictionary<string, FunctionInfo>()
            {
                ["Ping"] = new FunctionInfo("http://localhost/Ping", "GET", "application/json")
            };

            var client = new Client<Requester, JsonSerilizer>(responseHandler, functions);

            //здесь тело ответа пустое (предположим), по этому валидация не произведется
            client.Request("Ping");

            client.AddFunction("GetModel", new FunctionInfo("http://localhost/GetModel", "GET", "application/json"));
            //после получения ответа произойдет валидация модели и вывод на экран
            client.Request("GetModel");

            client.AddFunction("PostData", new FunctionInfo("http://localhost/PostData", "POST", "application/json"));
            client.Request<Input>("PostData", new Input { Id = 2, Namae = "Adsd" });
        }
    }
}
